import { SaberPro } from "./saberpro";
import { Saber } from "./saber";

export interface Estudiante {
    id: string;
    nombre: string;
    saberPro: SaberPro;
    saber: Saber;
}
